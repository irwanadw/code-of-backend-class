// This is Bangun Class
class Bangun {
  constructor(name) {
    this.name = name
  }

  menghitungVolume() {
    console.log('Menghitung Volume');
  }

  menghitungLuas() {
    console.log('Menghitung Luas');
  }

  menghitungKeliling() {
    console.log('Menghitung Keliling');
  }
}

module.exports = Bangun;
